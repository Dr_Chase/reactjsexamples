import React, {Component} from 'react';
import RepoList from './RepoList.jsx';

class Repo extends Component {

  render () {
    return(
      <li>
        <a href={repo.html_url}>
          {repo.name}
        </a> : {repo.description}
      </li>
    )
  }
}
export default Repo
