import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Profile from './github/Profile.jsx';
//import RepoList from './github/RepoList.jsx';

class App extends Component{
  constructor(props) {
    super(props);
    this.state = {
      username: 'dr-chase-g3ka1',
      userData: [],
      userRepos: [],
      perPage: 5
    }
  }

// Get user data from github
getUserData() {
  $.ajax({
    url: 'https://api.github.com/users/' + this.state.username + '?client_id='
    + this.props.clientId + '&client_secret='+this.props.clientSecret,
    dataType: 'json',
    cache: false,
    success: function(data) {
      this.setState({userData: data});
      console.log(data);
    }.bind(this),
    error: function(xhr, status, err){
      this.setState({username: null});
      alert(err);
    }.bind(this)

  });
}
// Get user repos
getUserRepos() {
  $.ajax({
    url: 'https://api.github.com/users/' + this.state.username +
    '/repos?per_page='+this.state.perPage+'&client_id='
    + this.props.clientId + '&client_secret='+this.props.clientSecret
    + '&sort=created',
    dataType: 'json',
    cache: false,
    success: function(data) {
      this.setState({userRepos: data});
    }.bind(this),
    error: function(xhr, status, err){
      this.setState({username: null});
      alert(err);
    }.bind(this)

  });
}
  componentDidMount() {
    this.getUserData();
    this.getUserRepos();
  }

  render()  {
    return(
      <div>
        <Profile {...this.state} />
      </div>
    )
  }
}

App.propTypes = {
  clientId: React.PropTypes.string,
  clientSecret: React.PropTypes.string
};
App.defaultProps ={
  clientId: '96147f12a3d2e9fbc3b2',
  clientSecret: 'ff515579e1ab04b79e84d0fc9f2f801fb696c5be'
}

export default App
// line 61 <Profile  userData = {this.state.userData}/> equals <Profile {...this.state} />
// this is how we pass data to other modules
