import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App.jsx'; // first letter upperCase convention
//import Profile from './github/Profile.jsx';

ReactDOM.render (
  <App />,
  document.getElementById('app')
);
